package models

import (
	"time"
)

type AuditTrail struct {
	Id        uint      `json:"id" gorm:"primaryKey"`
	CreatedAt time.Time `json:"created_at"`
	Entity    string    `json:"entity"`
	Actor     string    `json:"user"`
	Action    string    `json:"action"`
	OldData   string    `json:"old_data"`
	NewData   string    `json:"new_data"`
}

func (a AuditTrail) TableName() string {
	return "audit.audit_trail"
}
