package models

import (
	"time"
)

type Permission struct {
	Id        uint           `json:"id" gorm:"primaryKey"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	Name      string         `json:"name" gorm:"unique"`
}

func (p Permission) TableName() string {
	return "user_mgt.permission"
}
