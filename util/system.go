package util

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"sidat-audit/database"
	"sidat-audit/models"
)

func SendToAudit(c *fiber.Ctx, tableName string, action string, oldData interface{}, newData interface{}) (bool, error) {
	jwtUser := c.Locals("user").(*jwt.Token)
	jwtClaims := jwtUser.Claims.(jwt.MapClaims)

	cFlat, _ := json.Marshal(&jwtClaims)

	oldDataStr:=[]byte("")
	if oldData != nil {
		oldDataStr, _ = json.Marshal(oldData)
	}

	newDataStr := []byte("")
	if newData != nil {
		newDataStr, _ = json.Marshal(newData)
	}

	newLog := models.AuditTrail{
		Entity: tableName,
		Actor:     string(cFlat),
		Action:    action,
		OldData:   string(oldDataStr),
		NewData:   string(newDataStr),
	}

	result := database.DB.Create(&newLog)
	if result.Error != nil || result.RowsAffected <= 0 {
		return false, result.Error
	}

	return true, nil
}
