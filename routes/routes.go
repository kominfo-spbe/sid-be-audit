package routes

import (
	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"os"
	"sidat-audit/controllers"
)

func Setup(app *fiber.App) {
	// must be logged-in
	//app.Use(middleware.IsAuthenticated)
	app.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte(os.Getenv("TOKEN_SECRET")),
	}))

	app.Get("/api/audit-trail", controllers.AllAuditTrail)
	app.Get("/api/audit-trail/:id", controllers.GetAuditTrail)
}
