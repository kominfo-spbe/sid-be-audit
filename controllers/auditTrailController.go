package controllers

import (
	"github.com/gofiber/fiber/v2"
	"math"
	"sidat-audit/database"
	"sidat-audit/models"
	"sidat-audit/models/reqresp"
	"strconv"
)

// AllAuditTrail AuditTrail list
// @Summary AuditTrail list
// @Description AuditTrail list
// @Tags Audit Trail
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /audit-trail [get]
func AllAuditTrail(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	var records []models.AuditTrail

	database.DB.Offset(offset).Limit(limit).Find(&records)

	database.DB.Model(&models.AuditTrail{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// GetAuditTrail Get single AuditTrail data
// @Summary Get single AuditTrail data
// @Description Get single AuditTrail data
// @Tags Audit Trail
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "AuditTrail ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /audit-trail/{id} [get]
func GetAuditTrail(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.AuditTrail{Id: uint(id)}

	result := database.DB.Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}
