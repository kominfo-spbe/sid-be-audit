package database

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"os"
	"sidat-audit/models"
)

var DB *gorm.DB

func Connect() {
	db, err := gorm.Open(postgres.Open(os.Getenv("DSN")), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			//TablePrefix:   "audit.", // this is the schema
			SingularTable: true,
		},
	})

	if err != nil {
		panic(fmt.Errorf("Fatal error connect DB: %w \n", err))
	}

	fmt.Println("Db connect success")
	DB = db
	//DB.Exec(`set search_path='public'`)

	db.AutoMigrate(&models.AuditTrail{})
}
