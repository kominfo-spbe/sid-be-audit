package main

import (
	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/etag"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/joho/godotenv"
	"log"
	"os"
	"sidat-audit/database"
	_ "sidat-audit/docs"
	"sidat-audit/routes"
	"sidat-audit/util"
)

// @title BE Audit trail Sidatinkum
// @version 2021.9.21.1
// @description Audit Trail API untuk Sidatinkum

// @contact.name Pai Joe
// @contact.email pai.joe@wedibojone.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /api
func main() {

	// load .env
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// connect to DB
	database.Connect()

	app := fiber.New(fiber.Config{
		// Override default error handler
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			if e, ok := err.(*util.Error); ok {
				return ctx.Status(e.Status).JSON(e)
			} else if e, ok := err.(*fiber.Error); ok {
				return ctx.Status(e.Code).JSON(util.Error{Status: e.Code, Code: "internal-server", Message: e.Message})
			} else {
				return ctx.Status(500).JSON(util.Error{Status: 500, Code: "internal-server", Message: err.Error()})
			}
		},
	})

	// setup middleware
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	app.Use(compress.New(compress.Config{
		Level: compress.LevelBestSpeed, // 1
	}))
	app.Use(etag.New())
	app.Use(recover.New())
	app.Use(logger.New())

	// routing for swagger docs
	app.Get("/docs/*", swagger.Handler)

	// routing for others
	routes.Setup(app)

	app.Listen(":" + os.Getenv("PORT"))
}
